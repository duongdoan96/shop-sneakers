import React from 'react'

function LoadMore() {
  return (
    <div className="loadmore">
      <i className="fas fa-redo"></i>
      LOAD MORE
    </div>
  )
}

export default LoadMore
