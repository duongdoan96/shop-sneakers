import React from 'react'

interface ProductItemType {
  name: string,
  price: string,
}

function ProductItem(props ) {
  return (
    <div className="product-item">
      <div className="product-item__wrap">
        <div className="product-item__fake-img">275x205</div>
        <div className="product-item__wrap__info">
          <h5>KD 8 EXT</h5>
          <span>$ 145.00</span>
        </div>
      </div>

      <div className="product-item__hover">
        <h6>LIFESTYLE</h6>
        <button><i className="fas fa-cart-arrow-down"></i>Buy</button>
        <div className="hover__icon--copy">
          <a href="">
            <i className="far fa-copy"></i>
          </a>
        </div>
        <div className="hover__icon--heart">
          <a href="">
            <i className="far fa-heart"></i>
          </a>
        </div>
        <h4>Nike Air Footscape Magista</h4>
        <span>$ 235.00</span>
      </div>
    </div>
  )
}

export default ProductItem
