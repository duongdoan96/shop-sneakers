import React from 'react';
import iconBrand from '../../assets/images/icon-brand.png'

function BrandBox250() {
  return (
    <div className="brand-box-250">
      <img src={iconBrand} alt="" />
    </div>
  )
}

export default BrandBox250
