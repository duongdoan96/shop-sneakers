import React from 'react'

interface CardDirectType {
  url : string,
  title: string,
}

function CardDirect(props: CardDirectType) {
  return (
    <div className="card-direct">
      <img src={props.url} alt="" />
      <h3>{props.title}</h3>
      <p>Suspendisse massa leo, vesti Suspendisse massa leo, </p>
      <a href="">More Product
        <i className="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  )
}

export default CardDirect
