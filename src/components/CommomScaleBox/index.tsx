import React from 'react'
import { Button } from 'reactstrap'

interface CommomScaleBoxType {
  type: string;
  name: string;
  width: string,
  bgColor: string
}

function CommomScaleBox(props: CommomScaleBoxType) {
  return (
    <div className="commom-scale-box" style={{
        width: `${props.width}`,
        backgroundColor: `${props.bgColor}`
      }}>
        <div className="commom-scale-box__wrap">
      <p>{props.type}</p>
      <h1>{props.name}</h1>
      <Button className="btn-commom" color="primary" style={{backgroundColor: `${props.bgColor}`}}>More Info</Button>
        </div>
    </div>
  )
}

export default CommomScaleBox
