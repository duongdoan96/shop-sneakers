import React from 'react'

function CommentBox() {
  return (
    <div className="comment-box">
      <div className="user">
        <div className="user__img"></div>
        <div className="user__name">
          <h4>Meafan Disher</h4>
          <ul>
            <li><i className="fas fa-star"></i></li>
            <li><i className="fas fa-star"></i></li>
            <li><i className="fas fa-star"></i></li>
            <li><i className="fas fa-star"></i></li>
            <li><i className="fas fa-star-half-alt"></i></li>
          </ul>
        </div>
      </div>

      <div className="comment">
        <p>Proin finibus lorem enim. Vestibulum consequat cursus diam, id Proin finibus lorem enim. Vestibulum consequat cursus diam, id Proin finibus lorem enim. Vestibulum consequat cursus diam, id </p>
        <div className="comment__like">
          <div className="comment__like__wrap">
            <div style={{color: 'green'}}><i className="fas fa-thumbs-up"></i>12</div>
            <div style={{color: 'red'}}><i className="fas fa-thumbs-down"></i>15</div>
          </div>
          <span>25 sep 2016</span>
        </div>
      </div>
    </div>
  )
}

export default CommentBox
