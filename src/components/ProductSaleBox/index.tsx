import React from 'react'

function ProductSaleBox() {
  return (
    <div className="product-sale-box">
      <span>DEAL OF DAY</span>
      <h2>Acronym Jacket: J1A-GT</h2>
      <p>Suspendisse massa leo, vesti Suspendisse massa leo, vesti Suspendisse massa leo, vesti Suspendisse massa leo, vesti Suspendisse massa leo, vesti Suspendisse massa leo, vestiSuspendisse massa leo, vestiSuspendisse massa leo, vesti</p>
      <div className="product-sale-box__money">
        <h1>$1235</h1>
        <span>$1890</span>
      </div>
      <div className="product-sale-box__times">
        <div className="product-sale-box__times__item">
          <h4>01</h4>
          <span>DAY</span>
        </div>
        <div className="product-sale-box__times__item">
          <h4>23</h4>
          <span>HOURS</span>
        </div>
        <div className="product-sale-box__times__item">
          <h4>55</h4>
          <span>MINS</span>
        </div>
        <div className="product-sale-box__times__item">
          <h4>32</h4>
          <span>SECS</span>
        </div>
      </div>
    </div>
  )
}

export default ProductSaleBox
