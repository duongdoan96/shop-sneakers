import React from 'react';
import {Link} from 'react-router-dom'
import Productitem from '../../components/ProductItem'
import iconList from '../../assets/images/icon-category-list.png'
import CategorySidebar from '../../components/CategorySidebar'
import CommomScaleBox from '../../components/CommomScaleBox'
import SubBanner from '../../components/SubBanner/index'
import Header from '../../layouts/MainLayout/Header/index'

function CategoryPage() {
  return (
    <>
      <Header isHomepage={false} />
      <SubBanner />
      <div className="category-page">
        <section className="wrap__show-product">
          <CommomScaleBox type="MOUNTAIN BIKE" name="Trek Speed Concept" width="67%" bgColor="#61ade2"/>
          <CommomScaleBox type="MOUNTAIN BIKE" name="Sale Collection 2015" width="33%" bgColor="#7d6de5"/>
        </section>

        <section className="filter">
          <h3>Filter</h3>
          <div className="filter__refresh">
            <Link to="">
              <i className="fas fa-sync-alt"></i>
            </Link>
          </div>

          <div className="filter__sort">
            <div className="sort__list">
              <h6>SORT BY: Price $-$$</h6>
              <i className="fas fa-sort-down"></i>
            </div>
            <div className="sort__item">
              {/* list item in here */}
            </div>
          </div>

          <div className="filter__show">
            <div className="show__list">
              <h6>SHOW: 12</h6>
              <i className="fas fa-sort-down"></i>
            </div>
            <div className="show__item">
              {/* list item in here */}
            </div>
          </div>

          <div className="filter__all">
            <Link to="">
              <img src={iconList} alt="" />
            </Link>
          </div>

          <div className="filter__list">
            <Link to="">
              <i className="fas fa-list"></i>
            </Link>
          </div>

          <div className="filter__compare">
            <h6>COMPARE: </h6>
            <span>3</span>
          </div>
        </section>

        <div className="category__wrap">
          <section className="sidebar">
            <CategorySidebar name="CATEGORIES" showListCate  />
            <CategorySidebar name="PRICE" showPrice />
            <CategorySidebar name="BRAND" showRadio />
            <CategorySidebar name="COLORS" showColor />
            <CategorySidebar name="SIZE" showTable />
          </section>

          <section className="category__wrap__list-product">
            <Productitem />
            <Productitem />
            <Productitem />

            <Productitem />
            <Productitem />
            <Productitem />

            <Productitem />
            <Productitem />
            <Productitem />

            <Productitem />
            <Productitem />
            <Productitem />
          </section>
        </div>

        <section className="category__panagation">
          <div className="panagation__space"></div>
          <div className="panagation__main">
            <div className="panagation__main__left">
              <Link to="">
                <i className="fas fa-chevron-left"></i>
              </Link>
            </div>

            <ul>
              <li><Link to="">1</Link></li>
              <li><Link to="">2</Link></li>
              <li><Link to="">3</Link></li>
              <li><Link to="">4</Link></li>
              <li><Link to="">5</Link></li>
            </ul>

            <div className="panagation__main__right">
              <Link href="#">
                <i className="fas fa-chevron-right"></i>
              </Link>
            </div>
          </div>
        </section>
      </div>
    </>
  )
}

export default CategoryPage
