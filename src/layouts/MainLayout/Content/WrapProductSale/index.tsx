import ImgShowProductBox from '../../../../components/ImgShowProductBox/index'
import ProductSaleBox from '../../../../components/ProductSaleBox/index'
import React from 'react'

function WrapProductSale() {
  return (
    <section className="wrap-product-sale">
      <ProductSaleBox />
      <ImgShowProductBox />
    </section>
  )
}

export default WrapProductSale
