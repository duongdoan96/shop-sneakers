import CardDirect from '../../../../components/CardDirect/index'
import Productitem from '../../../../components/ProductItem/index';
import iconStar from '../../../../assets/images/icon-card-direct-star.png';
import CommomScaleBox from '../../../../components/CommomScaleBox/index';

import React from 'react';

interface WrapProductCommomType {
  isCardDirect: boolean;
  isCommomScaleBox: boolean;
  isNormal: boolean
}

function WrapProductCommom(props :WrapProductCommomType) {
  return (
    <>
      {
        props.isCardDirect ?
        <div className="wrap-product-item">
          <CardDirect url={iconStar} title="POPULAR PRODUCTS" />
          <Productitem />
          <Productitem />
          <Productitem />
        </div> : ''
      }
      {
        props.isCommomScaleBox ?
        <div className="wrap-scale-box">
          <Productitem />
          <Productitem />
          <CommomScaleBox type="LIFESTYLE" name="WATERFLY: Running Shoes" width="50%" bgColor="#50a3e4" />
        </div>
        : ''
      }
      {
        props.isNormal ?
        <div className="wrap-product-normal">
          <Productitem />
          <Productitem />
          <Productitem />
          <Productitem />
        </div> : ''
      }
    </>
  )
}

export default WrapProductCommom
