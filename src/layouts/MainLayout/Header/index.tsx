import React from 'react'

interface HeaderType {
  isHomepage: boolean
}

const Header = (props: HeaderType) => {
  return (
    <div className='header'>
        <div className="wrapped" style={{backgroundColor: `${props.isHomepage ? '#454957': '#3a54d6'}`}}>
          <div className="header__search">
            <a href="#">
              <i className="fas fa-search"></i>
            </a>
          </div>
          <div className="header__category">
            <div className="header__category__icon">
              <a href="#">
                <i className="fas fa-bars"></i>
              </a>
            </div>
            <div className="header__category_name">
              <p>Select</p>
              <span>Category</span>
            </div>
          </div>
          <div className="header__logo">
            <h2>AGORA</h2>
          </div>
          <div className="header__user">
            <a href="#">
              <i className="far fa-user"></i>
            </a>
          </div>
          <div className="header__like">
            <a href="#">
              <i className="far fa-heart"></i>
            </a>
          </div>
          <div className="header__cart">
            <a href="">
              <i className="fas fa-shopping-cart"></i>
            </a>
          </div>
        </div>
    </div>
  )
}

export default Header;
